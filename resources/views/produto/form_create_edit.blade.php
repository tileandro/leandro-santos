@if (isset($produto->id))
<form method="post" class action="{{ route('produtos.update', $produto['id']) }}">
    @csrf
    @method('PUT')
    <input type="hidden" name="produto_id" value="{{$produto->id}}" />
@else
<form method="post" action="{{ route('produtos.store') }}">
    @csrf
@endif
    <div class="form-group mb-3">
        <input type="text" class="form-control" value="{{ $produto->nome_produto ?? old('nome_produto') }}" id="nome_produto" name="nome_produto"
        placeholder="Nome do Produto">
    </div>
    <div class="form-group mb-3">
        <input type="text" class="form-control" value="{{ $produto->valor_unitario ?? old('valor_unitario') }}" id="valor_unitario" name="valor_unitario"
        placeholder="Valor unitário">
    </div>
    <div class="form-group mb-3">
        <input type="number" class="form-control" value="{{ $produto->cod_barras ?? old('cod_barras') }}" id="cod_barras" name="cod_barras"
        placeholder="Cod barras">
    </div>
    <div class="form-group mt-3">
        <button type="submit" class="btn btn-primary">@if (isset($produto->id)) Editar @else Cadastrar @endif Produto </button>
    </div>
</form>
