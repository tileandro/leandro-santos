@if (isset($cliente->id))
<form method="post" class action="{{ route('clientes.update', $cliente['id']) }}">
    @csrf
    @method('PUT')
    <input type="hidden" name="cliente_id" value="{{$cliente->id}}" />
@else
<form method="post" action="{{ route('clientes.store') }}">
    @csrf
@endif
    <div class="form-group mb-3">
        <input type="text" class="form-control" value="{{ $cliente->nome_cliente ?? old('nome_cliente') }}" id="nome_cliente" name="nome_cliente"
        placeholder="Nome do Cliente">
    </div>
    <div class="form-group mb-3">
        <input type="email" class="form-control" value="{{ $cliente->email ?? old('email') }}" id="email" name="email"
        placeholder="E-mail do Cliente">
    </div>
    <div class="form-group mb-3">
        <input type="number" class="form-control" value="{{ $cliente->cpf ?? old('cpf') }}" id="cpf" name="cpf"
        placeholder="CPF do Cliente">
    </div>
    <div class="form-group mt-3">
        <button type="submit" class="btn btn-primary">@if (isset($cliente->id)) Editar @else Cadastrar @endif Cliente </button>
    </div>
</form>
