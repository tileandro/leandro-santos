<form method="post" action="{{ route('pedidos.store') }}">
    @csrf
    <div class="form-group mb-3">
        <select class="form-control" name="cliente_id" id="cliente_id">
            <option value="">Escolha o cliente</option>
            @foreach ($clientes as $cliente)
                <option value="{{$cliente['id']}}" {{ ($pedido->cliente_id ?? old('cliente_id')) == $cliente['id'] ? 'selected' : '' }}>{{$cliente['nome_cliente']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group mb-3">
        <select class="form-control" name="produto_id" id="produto_id">
            <option value="">Escolha o produto</option>
            @foreach ($produtos as $produto)
                <option value="{{$produto['id']}}" {{ ($pedido->produto_id ?? old('produto_id')) == $produto['id'] ? 'selected' : '' }}>{{$produto['nome_produto']}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group mb-3">
        <select class="form-control" name="status" id="status">
            <option value="">Escolha o status</option>
            <option value="Em Aberto" {{ ($pedido->status ?? old('status')) == "Em Aberto" ? 'selected' : '' }}>Em Aberto</option>
            <option value="Pago" {{ ($pedido->status ?? old('status')) == "Pago" ? 'selected' : '' }}>Pago</option>
            <option value="Cancelado" {{ ($pedido->status ?? old('status')) == "Cancelado" ? 'selected' : '' }}>Cancelado</option>
        </select>
    </div>
    <div class="form-group mb-3">
        <input type="number" class="form-control" value="{{ $pedido->quantidade ?? old('quantidade') }}" id="quantidade" name="quantidade"
        placeholder="Quantidade">
    </div>
    <div class="form-group mt-3">
        <button type="submit" class="btn btn-primary">Cadastrar Pedido </button>
    </div>
</form>
