@extends('layouts.app')

@section('content')
<div class="container">
    <h1>{{$titulo}}</h1>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="col">
                <div class="mt-10">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mb-3" role="alert">
                        @foreach ($errors->all() as $erro)
                            <p>{{ $erro }}</p>
                        @endforeach
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert alert-success alert-dismissible fade show mb-3" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <table class="table table-sm table-hover">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">ID PEDIDO</th>
                                <th scope="col">ID CLIENTE</th>
                                <th scope="col">ID PRODUTO</th>
                                <th scope="col">STATUS</th>
                                <th scope="col">QTD</th>
                                <th scope="col">DATA DE CRIAÇÃO</th>
                                <th scope="col">DATA DE ATUALIZAÇÃO</th>
                                <th scope="col">
                                    <div class="form-group">
                                        <a href="{{route('pedidos.create')}}" class="btn btn-primary btn-sm">Cadastrar Pedido</a>
                                    </div>
                                </th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pedidos as $pedido)
                                <tr>
                                    <th scope="row">{{$pedido['id']}}</th>
                                    <td><a href="{{route('clientes.edit', $pedido['cliente_id'])}}" target="_blank">{{$pedido['cliente_id']}}</a></td>
                                    <td><a href="{{route('produtos.edit', $pedido['produto_id'])}}" target="_blank">{{$pedido['produto_id']}}</a></td>
                                    <td>{{$pedido['status']}}</td>
                                    <td>{{$pedido['quantidade']}}</td>
                                    <td>{{date('d/m/Y h:m:s', strtotime($pedido['created_at']))}}</td>
                                    <td>{{date('d/m/Y h:m:s', strtotime($pedido['updated_at']))}}</td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm deletar" data-toggle="modal" data-id="{{$pedido['id']}}" data-name="{{$pedido['id']}}" data-target="#modalDeletar">
                                            Deletar Pedido
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDeletar" tabindex="-1" role="dialog" aria-labelledby="TituloModalCentralizado" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title link-light" id="TituloModalCentralizado">Deletar Pedido</h5>
                    <button type="button btn-dark" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tem certeza que você deseja deletar o Pedido <b></b>?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary close" data-dismiss="modal">Cancelar</button>
                    <form method="post">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Deletar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.deletar, .close').click(function () {
                $('#modalDeletar').modal('toggle');
                $('.modal-body b').html($(this).attr("data-name"));
                $('.modal-footer form').attr("action", "/pedidos/" + $(this).attr("data-id") + "");
            });
        });
    </script>
</div>
@endsection
