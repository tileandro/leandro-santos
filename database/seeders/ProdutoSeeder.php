<?php

namespace Database\Seeders;

use App\Models\Produto;
use Illuminate\Database\Seeder;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $produto = new Produto();
        $produto->nome_produto = 'Smart TV';
        $produto->valor_unitario = '2000.00';
        $produto->cod_barras = '897564654683234';
        $produto->save();

        $produto = new Produto();
        $produto->nome_produto = 'Guarda roupa';
        $produto->valor_unitario = '9000.00';
        $produto->cod_barras = '7981865456';
        $produto->save();

        $produto = new Produto();
        $produto->nome_produto = 'Celular';
        $produto->valor_unitario = '1100.00';
        $produto->cod_barras = '561468545465';
        $produto->save();

        $produto = new Produto();
        $produto->nome_produto = 'Geladeira';
        $produto->valor_unitario = '4999.99';
        $produto->cod_barras = '545454545';
        $produto->save();

        $produto = new Produto();
        $produto->nome_produto = 'Bicicleta';
        $produto->valor_unitario = '756.90';
        $produto->cod_barras = '852477866';
        $produto->save();
    }
}
