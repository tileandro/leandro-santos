<?php

namespace Database\Seeders;

use App\Models\Cliente;
use Database\Factories\ClienteFactory;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Cliente::factory()->count(100)->create();
    }
}
