<?php

namespace App\Http\Controllers;

use App\Models\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::all();
        return view('produto.index', ['titulo' => 'Produtos', 'produtos' => $produtos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produto.create', ['titulo' => 'Cadastrar Produto']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nome_produto' => 'required|min:3|max:100',
                'valor_unitario' => 'required',
                'cod_barras' => 'required|max:20'
            ],
            [
                'required' => 'Campo :attribute é obrigatório seu preenchimento',
                'nome_produto.min' => 'O nome do produto deve ter no mínimo 3 caracteres',
                'nome_produto.max' => 'O nome do produto deve ter no máximo 100 caracteres',
                'cod_barras.max' => 'O cod de barras deve ter no máximo 20 caracteres'
            ]
        );

        Produto::create($request->all());
        return back()->with('success', 'Produto ' . $request->input('nome_produto') . ' cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function show(Produto $produto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function edit(Produto $produto)
    {
        return view('produto.edit', ['titulo' => "Editar Produto", 'produto' => $produto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produto $produto)
    {
        $request->validate(
            [
                'nome_produto' => 'required|min:3|max:100',
                'valor_unitario' => 'required',
                'cod_barras' => 'required|max:20'
            ],
            [
                'required' => 'Campo :attribute é obrigatório seu preenchimento',
                'nome_produto.min' => 'O nome do produto deve ter no mínimo 3 caracteres',
                'nome_produto.max' => 'O nome do produto deve ter no máximo 100 caracteres',
                'cod_barras.max' => 'O cod de barras deve ter no máximo 20 caracteres'
            ]
        );

        $produto->update($request->all());
        return back()->with('success', 'Produto ' . $request->input('nome_produto') . ' editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produto  $produto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produto $produto)
    {
        $produto->delete();
        return back()->with('success', 'Produto deletado com sucesso!');
    }
}
