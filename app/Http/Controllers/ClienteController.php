<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::all();
        return view('cliente.index', ['titulo' => 'Clientes', 'clientes' => $clientes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cliente.create', ['titulo' => 'Cadastrar Cliente']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nome_cliente' => 'required|min:3|max:100',
                'email' => 'email|max:50',
                'cpf' => 'required|max:11'
            ],
            [
                'required' => 'Campo :attribute é obrigatório seu preenchimento',
                'nome_cliente.min' => 'O nome do cliente deve ter no mínimo 3 caracteres',
                'nome_cliente.max' => 'O nome do cliente deve ter no máximo 100 caracteres',
                'email.max' => 'Campo e-mail é permitido até 50 caracteres',
                'email.unique' => 'E-mail já cadastrado, por favor digite outro e-mail.'
            ]
        );

        Cliente::create($request->all());
        return back()->with('success', 'Cliente ' . $request->input('nome_cliente') . ' cadastrado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function show(Cliente $cliente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function edit(Cliente $cliente)
    {
        //$cliente = Cliente::all();
        return view('cliente.edit', ['titulo' => "Editar Cliente", 'cliente' => $cliente]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        $request->validate(
            [
                'nome_cliente' => 'required|min:3|max:100',
                'email' => 'email|max:50',
                'cpf' => 'required|max:11'
            ],
            [
                'required' => 'Campo :attribute é obrigatório seu preenchimento',
                'nome_cliente.min' => 'O nome deve ter no mínimo 3 caracteres',
                'nome_cliente.max' => 'O nome deve ter no máximo 50 caracteres',
                'email.max' => 'Campo e-mail é permitido até 100 caracteres',
                'email.unique' => 'E-mail já cadastrado, por favor digite outro e-mail.'
            ]
        );

        $cliente->update($request->all());
        return back()->with('success', 'Cliente ' . $request->input('nome_cliente') . ' editado com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cliente  $cliente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cliente $cliente)
    {
        $cliente->delete();
        return back()->with('success', 'Cliente deletado com sucesso!');
    }
}
